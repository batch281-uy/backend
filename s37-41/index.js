const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

//Connect to our MongoDB
mongoose.connect(
  "mongodb+srv://koberyan873:Lakers7926@wdc028-course-booking.qto9ey1.mongodb.net/s37-41API?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection Error."));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

// Allows all resources to access our backend application
app.use(cors());
// Add json middleware allows us to parse incoming request/ allow us to send request to our body to our routes
// allows us to parse json data
app.use(express.json());

// middleware parses form data that is going to submit via POST request
// this parses forms data coming from our forms
app.use(express.urlencoded({ extended: true }));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000}`);
});
