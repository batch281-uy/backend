// console.log("hello world");

// [Section] Javascript Syncrhonous vs Asynchronous
// JavaScript is by default sychronous meaning that only one statement is executed at a time.

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log("Hello!");
// console.log("Hello Again!");
console.log("Goodbye");

// When certain statements takes a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code "blocking"
// we might not notice it due to the fast proessing power of our device
// this is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Create a simple fetch request

// [Section] Getting all posts

// The fetch API allows you to asynchronously request for a resource(data)
// A "promise" is an object that represents the eventual completion (or failure) of an ascynchronous function and it's resulting value

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Check the status of the request

// Retrieve all posts following the REST API(retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")
  // The "fetch" method will return a "promise" that resolves to a "Response" object
  // The "then" method captures the "Response" object and returns another "promise" which will eventually be "Resolved" or "Rejected"
  .then((response) => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
  //   use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our collection
  .then((response) => response.json())
  // Print the converted JSON value from the "fetch" request
  // Using multiple "then" methods creates a promise chain
  .then((json) => console.log(json));

//   Demonstrate using the "async" and "await" keywords

// the "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// used in functions to indicate which portions of code should be waited for
// creates asynchronous function
async function fetchData() {
  // waits for the "fetch" method to complete then stores the value in the "result" variable
  let result = await fetch("https://jsonplaceholder.typicode.com/posts");
  // Result returned by fetch returns a promise
  console.log(result);
  // the returned "response" is an object
  console.log(typeof result);
  // we cannot access the content of the "response" by directly accessing its body's property
  console.log(result.body);

  // converts data from the "response" object as JSON
  let json = await result.json();
  // print out the content of the "response object"
  console.log(json);
}

fetchData();

// process a GET request using postman

/* 
    postman:
    url: https://jsonplaceholder.typicode.com/posts
    method: GET
  
*/

// [Section] getting a specific post

// retrieves a specific post following the REST API (retrieve, /posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Section] Creating a post

// creates a new post following the REST API(create, /post/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
  // sets the method of the "request" object to "POST" following the REST API
  // Default method is GET
  method: "POST",
  // sets the method of the "request object to "POST" following REST API
  // specified that the content will be in a JSON structure
  headers: {
    "Content-Type": "application/json",
  },
  //   sets the content/body data of the "request" object to be sent to the backend
  // JSON.stringify converts the object data into a strigified JSON
  body: JSON.stringify({
    title: "New post",
    body: "Hello World!",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Section] Updating a post

// updates a new post following the REST API(create, /post/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Updated post",
    body: "Hello World! Again",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//   [Section] Updating a post using PATCH

// UPDATEs a specific post following the REST API(update, /posts/:id, PATCH)
// the difference between PUT and PATCH is the number of poroperties being changed
// PATCH is used to update the whole object
// PUT is used to update a single/several properties
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Corrected post",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//   [Section] deleting a post

// deleting a specific post following the REST API
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
});

// [Section] filtering posts

// the data can be filtered by sending the userId along witht eh URL
// information ent via the url can be done by adding the question mark symbol(?)

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Section] Retrieving nested/related comments to posts

// Retrieving comments for a specific post following the REST API(retrieve, /posts/:id/comments)
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((response) => response.json())
  .then((json) => console.log(json));
