const http = require("http");

const items = [
  {
    productName: "Mug",
    stock: 50,
    isAvailable: true,
  },
  {
    productName: "Pencil",
    stock: 35,
    isAvailable: true,
  },
];

const port = 4000;

const app = http.createServer((request, response) => {
  if (request.url == "/items" && request.method == "GET") {
    response.writeHead(200, { "Content-Type": "text/json" });
    response.write(JSON.stringify(items));
    response.end();
  }

  if (request.url == "/addItem" && request.method == "POST") {
    let requestBody = "";
    request.on("data", (data) => {
      requestBody += data;
    });

    request.on("end", () => {
      console.log(typeof requestBody);
      requestBody = JSON.parse(requestBody);

      const newItem = {
        productName: requestBody.productName,
        stock: requestBody.stock,
        isAvailable: requestBody.isAvailable,
      };
      items.push(newItem);
      console.log(items);

      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(newItem));
      response.end();
    });
  }
});

app.listen(port, () => console.log("Server running at localhost:4000"));
