/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.*/

function sumOf2Nums(num1, num2){
	let sum =num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}

sumOf2Nums(5, 15);

function addNum(num1, num2) {
	return num1 + num2;
}

console.log(addNum(5, 10))
/*

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

function differenceOf2Nums(num1, num2){
	let diff =num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(diff);
}

differenceOf2Nums(20, 5);

/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.*/

/*		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.
*/
/*	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
function productOf2Nums(num1, num2){
	let product =num1 * num2;
		console.log("The product of " + num1 + " and " + num2 + ":");
		return product;
}

let myProduct = productOf2Nums(50, 10);
console.log(myProduct);


function quotientOf2Nums(num1, num2){
	let quotient =num1 / num2;
		console.log("The quotient of " + num1 + " and " + num2 + ":");
		return quotient;
}

let myQuotient = quotientOf2Nums(50, 10)
console.log(myQuotient)






/*	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/
function circleArea(radius){
	let area = (22/7) * (radius**2);
	console.log("The result of gettng the area of a circle with " + radius +" radius:");
	return area;
}

let myArea = circleArea(15);
console.log(myArea);



/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

function averageVar(num1, num2, num3, num4){
	let averageOf4 =(num1 + num2 + num3 + num4) / 4;
	console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	return averageOf4;
}

let myAverage = averageVar(20, 40, 60, 80);
console.log(myAverage)



/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

function isPassingScore(score, total){
	let percentage = score / total;
	console.log("Is " + score + "/" + total + " a passing score?")
	let isPassed = percentage >= .75;
	return isPassed;
}

let myScore = isPassingScore(38, 50);
console.log(myScore)