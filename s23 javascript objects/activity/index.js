/* Activity:
 1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.

 2. Link the script.js file to the index.html file.

 3. Create a trainer object using object literals.
 
 4. Initialize/add the following trainer object properties:
 - Name (String)
 - Age (Number)
 - Pokemon (Array)
 - Friends (Object with Array values for properties)





 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!*/
let trainer = { 
    name: "Ash Kethchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {hoenn: ["Mary", "Max"], kanto: ["Brock", "Misty"]},
    talk: function(){
        console.log('Pikachu! I choose you!')
    }
}



console.log(typeof trainer);



/* 6. Access the trainer object properties using dot and square bracket notation.
*/
console.log('Result of dot notation: ');
console.log(trainer.name);
console.log('Result of square bracket notation: ');
console.log(trainer["pokemon"]);

 /*7. Invoke/call the trainer talk object method.*/
console.log('Result of talk method')
trainer.talk();
 
 /*8. Create a constructor for creating a pokemon with the following properties:
 - Name (Provided as an argument to the contructor)
 - Level (Provided as an argument to the contructor)
 - Health (Create an equation that uses the level property)
 - Attack (Create an equation that uses the level property)
*/
 function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function(target) {
        let _targetPokemonHealth_ = target.health - this.attack;   
    console.log(this.name + ' tackled ' + target.name);
    console.log(target.name + " health is now reduced to " + _targetPokemonHealth_);
        target.health = _targetPokemonHealth_;
        if (_targetPokemonHealth_ <= 0){
            this.faint(target);
            
      }  
       
    }

         this.faint = function(target) {
             console.log(target.name + ' fainted.');
    }
}

 /*9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
*/
let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16, 8);
let meotwo = new Pokemon("Meotwo", 100, 200, 100);




/* 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.

    



 11. Create a faint method that will print out a message of targetPokemon has fainted.

 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.

 13. Invoke the tackle method of one pokemon object to see if it works as intended.

 14. Create a git repository named S23.

 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

 16. Add the link in Boodle. // console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}

*/