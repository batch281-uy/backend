// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");
		let yourAge = prompt("What is your age: ");
		let yourAddress = prompt("Your city of residence: ");
		console.log("Hello, " + firstName + " " + lastName);
		console.log("You are " + yourAge + " years old.");
		console.log("you live in " + yourAddress + " City");
	}

	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteArtists(){
		let favoriteArtists1 = "Doobie Brothers";
		let favoriteArtist2 = "Foo Fighters";
		let favoriteArtist3 = "Sting";
		let favoriteArtist4 = "Missy Elliot";
		let favoriteArtist5 = "the Calling";

		console.log("1. " + favoriteArtists1);
		console.log("2. " + favoriteArtist2);
		console.log("3. " + favoriteArtist3);
		console.log("4. " + favoriteArtist4);
		console.log("5. " + favoriteArtist5);
	}

	favoriteArtists();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
		let favoriteMovie1 = "John Wick 4";
		let favoriteMovie2 = "John Wick 1";
		let favoriteMovie3 = "John Wick 2";
		let favoriteMovie4 = "JOhn Wick 3";
		let favoriteMovie5 = "Nobody";

		console.log("1. " + favoriteMovie1);
		console.log("Rotten Tomatoes Rating: 94%")
		console.log("2. " + favoriteMovie2);
		console.log("Rotten Tomatoes Rating: 86%");
		console.log("3. " + favoriteMovie3);
		console.log("Rotten Tomatoes Rating: 89%");
		console.log("4. " + favoriteMovie4);
		console.log("Rotten Tomatoes Rating: 89%");
		console.log("5. " + favoriteMovie5);
		console.log("Rotten Tomatoes Rating: 84%");
	}

	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	let printFriends = //function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");


	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



