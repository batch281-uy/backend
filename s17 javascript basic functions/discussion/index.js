// Function
/*
	Syntax:
		function functionName(){
			code block (statement)
		}
*/
function printName(){
	console.log("My name is Ryan");
};

printName();

// Function declaration vs expressions

// Function declaration
function declaredFunction() {
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression (anonymous func:func with noname)
let variableFunction = function(){
	console.log("Hello again!")
}

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction")
};

declaredFunction();

const constantFunction = function() {
	console.log("Initialized with const!")
};

constantFunction();
/*
constantFunction = function(){
	console.log("Cannot be re-assigned")
}

constantFunction();
*/

// Function scoping

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames() {
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


// Nested Functions

function myNewFunction() {
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
}

// nestedFunction();
myNewFunction();

// Function and Global scoped variables

// Global scoped variable

let globalName = "Joy";

function myNewFunction2(){
	let nameInside = "Kenzo";

	console.log(globalName);
	console.log(nameInside);
}

myNewFunction2();

// console.log(nameInside);

// Using alert()

alert("Hello, world!"); //this will run immediately when the page loads

function showSampleAlert(){
	alert("Hello, user!");
}

showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");

}

printWelcomeMessage();

/*
Functions naming convenions: use verb/action words
	-Function names should be definitive of the tast it will perform
	-Avoid generic names to avoid confusion within the code
	-Avoid pointless and inappropriate function names
	-Name your functions following camel casing
	-Do not use JS reserved keywords
*/





















