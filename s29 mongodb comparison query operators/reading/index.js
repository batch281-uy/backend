db.beans.insertMany([
  {
    name: "Cappuccino",
    size: "Tall",
    price: 250,
    inStock: true,
  },
  {
    name: "Latte",
    size: "Venti",
    price: 150,
    inStock: true,
  },
  {
    name: "Americano",
    size: "Venti",
    price: 180,
    inStock: false,
  },
  {
    name: "Espresso",
    size: "Tall",
    price: 220,
    inStock: true,
  },
]);

// get all venti size coffee
db.beans.find({ size: "Venti" });

// get all coffee that are in stock
db.beans.find({ inStock: true });

// get all in stock coffee where the price is greater than 150
db.beans.find({
  $and: [{ inStock: true }, { price: { $gt: 150 } }],
});

// get all in stock coffee where the price is less than 200
db.beans.find({
  $and: [{ inStock: true }, { price: { $lt: 200 } }],
});
