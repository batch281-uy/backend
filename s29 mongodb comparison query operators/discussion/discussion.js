// [Section] Comparison Query Operators

// $gt/$gte
/* 
    - Allow us to find documents that have field number values greater than or equal to a specified field
    - Syntax:
    db.collectionName.find({field: $gt: value});
    db.collectionName.find({field: $gte: value});
*/
db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 21 } });

// $lt/$lte
/* - Allow us to find documents that have field number values less than or equal to a specified field
    - Syntax:
    db.collectionName.find({field: $lt: value});
    db.collectionName.find({field: $lte: value});
*/
db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 21 } });

// $ne operator
/*
    -Allows us to find documents that have field numbers not equal to specified value.
    Syntax:
    db.collectionName.find({ field:  $ne: value});    
*/
db.users.find({ age: { $ne: 55 } });

// $in operator
/*
    -Allows us to find documents with specific match criteria one field using different values
    Sytnax:
    db.collectionName.find({field: $in: value});
*/
db.users.find({ lastName: { $in: ["Hawking", "Doe"] } });
db.users.find({ courses: { $in: ["HTML", "React"] } });

// Opposite of in is not in $nin
db.users.find({ courses: { $nin: ["HTML", "React"] } });

// [Section] Logical Query Operator
// $or operator
/*
    -Allows us to find documents that match a single criteria from multiple provided criteria
    -Syntax
    db.collectionName.find({ $or: {[}fieldA: ValueB], {fieldB: value C}]})
*/

db.users.find({ $or: [{ firstName: "Neil" }, { age: 21 }] });
db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 30 } }] });

// $and operator
/*
    -allows us to find documents matching multiple criteria in a single field
*/
db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 } }] });

// Field Projection
/*
    Retrieving documents are common operations that we do and by default MongoDb queries return the whole document as a resposne
    -when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish
    -to help with readability of the values returned, we can include/exclude fields from the response
*/

// inclusion
/*
    -allows us to include/add specific fields only when retrieving documents
    -the value provided is 1 to denote that the field is being included
    syntax:
    db.collectionName.find({criteria, {field: 1}})
*/
db.users.find(
  {
    firstName: "Jane",
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
);

// Exclusion
/*
 -allows us to exclude/remove specific fields only when retrieving documents
    -the value provided is 0 to denote that the field is being excluded
    syntax:
    db.collectionName.find({criteria, {field: 0}})
*/
db.users.find(
  {
    firstName: "Jane",
  },
  {
    contact: 0,
    departrment: 0,
  }
);
// Supressing the ID field
db.users.find(
  {
    firstName: "Jane",
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0,
  }
);

// Returning specific fields in Embedded documents
db.users.find(
  {
    firstName: "Jane",
  },
  {
    firstName: 1,
    lastName: 1,
    "contact.phone": 1,
  }
);

// Supressing specific fields in embedded documents
db.users.find(
  {
    firstName: "Jane",
  },
  {
    "contact.phone": 0,
  }
);

// Project specific array elements in returned array

// the $slice operator allows us to retrieve only 1 element that matches the search criteria

db.users.insertOne({
  namearr: [
    {
      namea: "juan",
    },
    {
      nameb: "tamad",
    },
  ],
});

db.users.find(
  {
    namearr: {
      namea: "juan",
    },
  },
  { namearr: { $slice: 1 } }
);

// [section] evaluation query operators
// $regex
/*
    -allows us to find docs that match a specific string pattern using regular expressions
    syntax:
    db.users.find({field: $regex: 'pattern, $options: '$optionValue'})
*/

// case sensitive query
db.users.find({ firstName: { $regex: "N" } });
db.users.find({ firstName: { $regex: "n" } });
db.users.find({ firstName: { $regex: "Steph" } });
//   .         .
/* Jane, Stephen*/
// case insensitive query
db.users.find({ firstName: { $regex: "j", $options: "i" } });
